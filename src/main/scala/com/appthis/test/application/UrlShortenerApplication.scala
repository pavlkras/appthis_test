package com.appthis.test.application

import org.springframework.boot.autoconfigure.SpringBootApplication

/**
  * url-shortener
  *
  * @author Pavel
  * @since 07-Jul-17
  */
@SpringBootApplication(scanBasePackages = Array("com.appthis.test"))
class UrlShortenerApplication {

}
