package com.appthis.test.application

import org.springframework.boot.SpringApplication

/**
  * url-shortener
  *
  * @author Pavel
  * @since 07-Jul-17
  */
object Runner extends App{
  /**
    * Application's entry point function.
    *
    * @param args Command-line arguments.
    */
  override def main(args: Array[String]): Unit = {
    SpringApplication.run(classOf[UrlShortenerApplication], args: _*)
  }
}
