package com.appthis.test.application

import javax.inject.Inject
import javax.sql.DataSource

import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.{EnableWebSecurity, WebSecurityConfigurerAdapter}
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder

/**
  * url-shortener
  *
  * @author Pavel
  * @since 08-Jul-17
  */
@Configuration
@EnableWebSecurity
class SecurityConfiguration @Inject()(bCryptPasswordEncoder: BCryptPasswordEncoder,
                                      dataSource: DataSource) extends WebSecurityConfigurerAdapter {
  private val USERS_QUERY: String = "SELECT name, password, 1 FROM users WHERE name = ?"
  private val AUTHORITIES_QUERY: String = "SELECT name, role FROM roles JOIN users ON roles.id = users.id WHERE name = ?"

  override def configure(auth: AuthenticationManagerBuilder): Unit = {
    auth.jdbcAuthentication().dataSource(dataSource)
      .usersByUsernameQuery(USERS_QUERY)
      .authoritiesByUsernameQuery(AUTHORITIES_QUERY)
      .passwordEncoder(bCryptPasswordEncoder)
  }

  override def configure(http: HttpSecurity): Unit = {
    http.authorizeRequests()
      .antMatchers("/user/**").permitAll()
      .antMatchers("/bookmark/**").authenticated()
      .and().csrf().disable().formLogin()
      .loginPage("/user/login")
      .failureUrl("/user/error")
      .defaultSuccessUrl("/bookmark/list")
      .usernameParameter("username")
      .passwordParameter("password")
      .and().logout()
  }
}
