package com.appthis.test.application

import org.springframework.context.annotation.{Bean, Configuration}
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter

/**
  * url-shortener
  *
  * @author Pavel
  * @since 08-Jul-17
  */
@Configuration
class WebMvcConfig extends WebMvcConfigurerAdapter {
 @Bean
 def passwordEncoder(): BCryptPasswordEncoder = new BCryptPasswordEncoder()
}
