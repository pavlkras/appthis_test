package com.appthis.test.application

import javax.sql.DataSource

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import org.hibernate.SessionFactory
import org.springframework.context.annotation.{Bean, Configuration}
import org.springframework.http.converter.HttpMessageConverter
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder

/**
  * url-shortener
  *
  * @author Pavel
  * @since 07-Jul-17
  */
@Configuration
class BeanDefinitions {
  @Bean
  def getSessionFactory(dataSource: DataSource): SessionFactory = new LocalSessionFactoryBuilder(dataSource).scanPackages("com.appthis.test.dao.entities").buildSessionFactory()

  @Bean
  def getMessageConverter(): HttpMessageConverter[Object] = {
    val mapper = new ObjectMapper()
    mapper.registerModule(DefaultScalaModule)
    mapper.setSerializationInclusion(JsonInclude.Include.NON_ABSENT)
    new MappingJackson2HttpMessageConverter(mapper)
  }
}
