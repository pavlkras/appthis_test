package com.appthis.test.controller

import java.net.MalformedURLException
import javax.inject.Inject

import com.appthis.test.services.UrlShortenerService
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{GetMapping, PathVariable, RequestMapping}
import org.springframework.web.servlet.view.RedirectView

/**
  * url-shortener
  *
  * @author Pavel
  * @since 08-Jul-17
  */
@Controller
@RequestMapping(value = Array("/"))
class UrlRedirectController @Inject()(urlShortenerService: UrlShortenerService) {

  @GetMapping(value = Array("{shortenedUrl}"))
  def reqirectToUrlPage(@PathVariable shortenedUrl: String,
                        authentication: Authentication): RedirectView = {
    val url = urlShortenerService.recreateUrlFromShortenedForUser(shortenedUrl, authentication.getName)
    new RedirectView(url)
  }
}
