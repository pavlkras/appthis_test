package com.appthis.test.controller

import java.net.MalformedURLException
import javax.inject.Inject

import com.appthis.test.services.exceptions.{InvalidParametersException, ResourceSavingException}
import com.appthis.test.services.model.BookmarkDto
import com.appthis.test.services.{UrlShortenerService, UrlValidationService}
import org.apache.commons.lang3.StringUtils
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation._

/**
  * url-shortener
  *
  * @author Pavel
  * @since 07-Jul-17
  */
@RestController
@RequestMapping(value = Array("/bookmark"))
class UrlShortenerController @Inject()(urlShortenerService: UrlShortenerService,
                                       urlValidationService: UrlValidationService) {

  @PostMapping(value = Array("/add", "/add/"))
  @throws[MalformedURLException]
  @throws[ResourceSavingException]
  def addNewBookmark(@RequestParam(value = "name", required = true) name: String,
                     @RequestParam(value = "url", required = true) url: String,
                     authentication: Authentication): String = {
    if (!urlValidationService.validateUrl(url))
      throw new MalformedURLException("URL syntax is not valid")

    urlShortenerService.addBookmarkForUser(name, url, authentication.getName)
  }

  @GetMapping(value = Array("/list", "/list/"))
  @ResponseBody
  def listAllBookmarks(authentication: Authentication): Iterable[BookmarkDto] = urlShortenerService.listAllForUser(authentication.getName)

  @DeleteMapping(value = Array("/delete","/delete/"))
  @throws[InvalidParametersException]
  def deleteBookmark(@RequestParam(value = "name", required = false) name: String,
                     @RequestParam(value = "url", required = false) url: String,
                     @RequestParam(value = "shortenedUrl", required = false) shortenedUrl: String,
                     authentication: Authentication): Unit = {
    if (StringUtils.isNotEmpty(name) && StringUtils.isNotEmpty(url))
      urlShortenerService.deleteBookmarkForUser(name, url, authentication.getName)
    else if (StringUtils.isNotEmpty(shortenedUrl))
      urlShortenerService.deleteBookmarkForUser(shortenedUrl, authentication.getName)
    else
      throw new InvalidParametersException("Invalid parameters passed to service")
  }
}
