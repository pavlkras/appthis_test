package com.appthis.test.controller

import javax.inject.Inject

import com.appthis.test.dao.entities.User
import com.appthis.test.services.UserService
import com.appthis.test.services.exceptions.ResourceSavingException
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{PostMapping, RequestMapping, RequestParam, ResponseStatus}

/**
  * url-shortener
  *
  * @author Pavel
  * @since 08-Jul-17
  */
@Controller
@RequestMapping(value = Array("/user"))
class UserController @Inject()(userService: UserService) {
  @PostMapping(value = Array("/registration"))
  @ResponseStatus(HttpStatus.OK)
  def registerUser(@RequestParam("username") name: String,
                   @RequestParam("password") password: String): Unit = {
    val existingUser = userService.findUserByName(name)

    existingUser match {
      case Some(_) => throw new ResourceSavingException("User with such name already exists")
      case None => val user = new User
        user.name = name
        user.password = password
        userService.saveUser(user)
        "Registered"
    }
  }
}
