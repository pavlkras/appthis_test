package com.appthis.test.dao

import com.appthis.test.dao.entities.User

/**
  * url-shortener
  *
  * @author Pavel
  * @since 08-Jul-17
  */
trait UserDao extends Dao[User]{
  def findByName(name: String): Option[User]
}
