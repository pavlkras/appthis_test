package com.appthis.test.dao.entities

import javax.persistence._

import scala.beans.BeanProperty

/**
  * url-shortener
  *
  * @author Pavel
  * @since 08-Jul-17
  */
@Entity
@IdClass(classOf[BookMarkUserId])
@Table(name = "bookmarks_users")
class BookmarkUser extends Serializable{
  @Id
  @Column(name = "user_id", nullable = false, updatable = false)
  @BeanProperty
  var userId: Long = _

  @Id
  @Column(name = "bookmark_id", nullable = false, updatable = false)
  @BeanProperty
  var bookmarkId: Long = _
}
