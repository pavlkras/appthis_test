package com.appthis.test.dao.entities

import javax.persistence._

import scala.beans.BeanProperty

/**
  * url-shortener
  *
  * @author Pavel
  * @since 07-Jul-17
  */
@Entity
@Table(name = "bookmarks", uniqueConstraints = Array(new UniqueConstraint(columnNames = Array("name", "url"))))
class Bookmark extends Serializable{
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bookmarksIdSequence")
  @SequenceGenerator(name = "bookmarksIdSequence", sequenceName = "bookmarks_id_seq", allocationSize = 1)
  @Column(name = "id", nullable = false, updatable = false)
  @BeanProperty
  var id: Long = _

  @Column(name = "name", length = 50, nullable = false, updatable = true)
  @BeanProperty
  var name: String = _

  @Column(name = "url", length = 2000, nullable = false, updatable = true)
  @BeanProperty
  var url: String = _
}