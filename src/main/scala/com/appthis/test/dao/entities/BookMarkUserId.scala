package com.appthis.test.dao.entities

/**
  * url-shortener
  *
  * @author Pavel
  * @since 08-Jul-17
  */
class BookMarkUserId extends Serializable{
  var userId: Long = _
  var bookmarkId: Long = _
}
