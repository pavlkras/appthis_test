package com.appthis.test.dao.entities

import javax.persistence._

import scala.beans.BeanProperty

/**
  * url-shortener
  *
  * @author Pavel
  * @since 08-Jul-17
  */
@Entity
@Table(name = "users")
class User extends Serializable{
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "usersIdSequence")
  @SequenceGenerator(name = "usersIdSequence", sequenceName = "users_id_seq", allocationSize = 1)
  @Column(name = "id", nullable = false, updatable = false)
  @BeanProperty
  var id: Long = _

  @Column(name = "name", length = 50, unique = true, updatable = false, nullable = false)
  @BeanProperty
  var name: String = _

  @Column(name = "password", length = 100, updatable = true, nullable = false)
  @BeanProperty
  var password: String = _
}