package com.appthis.test.dao.entities

import javax.persistence._

import scala.beans.BeanProperty
/**
  * url-shortener
  *
  * @author Pavel
  * @since 08-Jul-17
  */
@Entity
@Table(name = "roles")
class Role extends Serializable {
  @Id
  @Column(name = "id", updatable = false, nullable = false)
  @BeanProperty
  var id: Long = _

  @OneToOne
  @PrimaryKeyJoinColumn(name = "id", referencedColumnName = "id")
  @BeanProperty
  var user: User = _

  @Column(name = "role", length = 20, updatable = true, nullable = false)
  @BeanProperty
  var role: String= _
}
