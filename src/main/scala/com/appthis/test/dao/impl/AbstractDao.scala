package com.appthis.test.dao.impl

import java.io
import java.lang.reflect.{ParameterizedType, Type}
import javax.inject.Inject
import javax.transaction.Transactional

import com.appthis.test.services.exceptions.InvalidParametersException
import com.appthis.test.dao.Dao
import org.hibernate.{HibernateException, Session, SessionFactory}

import scala.collection.JavaConverters._

/**
  * url-shortener
  *
  * @author Pavel
  * @since 07-Jul-17
  */
abstract class AbstractDao[E] extends Dao[E] {

  @Inject var sessionFactory: SessionFactory = _

  protected val entityType: Class[E] = {
    val className: String = getClass.getName

    val parametrizedType: ParameterizedType = getClass.getGenericSuperclass.asInstanceOf[ParameterizedType]
    if (parametrizedType == null)
      throw new IllegalStateException(s"Could not detect entity type for DAO implementation $className")

    val types: Array[Type] = parametrizedType.getActualTypeArguments
    if (types == null || types.length != 1)
      throw new IllegalStateException(s"Could not detect entity type for DAO implementation $className")

    types(0).asInstanceOf[Class[E]]
  }

  override def findById(id: io.Serializable): Option[E] = {
    Option(id) match {
      case Some(_) => Option(getSession().get(entityType, id))
      case None => None
    }
  }

  @Transactional
  override def save(entity: E): io.Serializable = {
    Option(entity) match {
      case Some(_) => getSession().save(entity)
    }
  }

  override def list(): Iterable[E] = {
    getSession().createQuery(s"FROM ${entityType.getName}").list().asScala.map(_.asInstanceOf[E])
  }

  @Transactional
  override def delete(id: io.Serializable): Unit = {
    val entity = findById(id)
    entity match {
      case Some(_) => delete(entity.get)
      case None => throw new InvalidParametersException("No entity to delete was found")
    }
  }

  @Transactional
  override def delete(entity: E): Unit = getSession().delete(entity)

  protected def getSession(): Session = {
    try {
      sessionFactory.getCurrentSession
    } catch {
      case he: HibernateException => sessionFactory.openSession()
    }
  }
}
