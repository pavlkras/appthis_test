package com.appthis.test.dao.impl

import com.appthis.test.dao.RoleDao
import com.appthis.test.dao.entities.Role
import org.springframework.stereotype.Repository

/**
  * url-shortener
  *
  * @author Pavel
  * @since 08-Jul-17
  */
@Repository
class RoleDaoHibernate extends AbstractDao[Role] with RoleDao{
  override def findByRole(role: String): Option[Role] = {
    Option(getSession().createQuery("FROM Role r WHERE r.role = :role")
      .setParameter("role", role)
      .uniqueResult().asInstanceOf[Role])
  }
}
