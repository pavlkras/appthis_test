package com.appthis.test.dao.impl

import java.util

import com.appthis.test.services.exceptions.InvalidParametersException
import com.appthis.test.dao.BookmarkDao
import com.appthis.test.dao.entities.Bookmark
import org.hibernate.transform.ResultTransformer
import org.springframework.stereotype.Repository

import scala.collection.JavaConverters._

/**
  * url-shortener
  *
  * @author Pavel
  * @since 07-Jul-17
  */
@Repository
class BookmarkDaoHibernate extends AbstractDao[Bookmark] with BookmarkDao {
  override def delete(name: String, url: String): Unit = {
    val status = getSession().createQuery("DELETE FROM Bookmark b WHERE b.name = :name AND b.url = :url")
      .setParameter("name", name)
      .setParameter("url", url)
      .executeUpdate()

    if (status == 0)
      throw new InvalidParametersException("No entity to delete was found")
  }

  override def findByNameUrl(name: String, url: String): Option[Bookmark] = {
    Option(getSession().createQuery("FROM Bookmark b WHERE b.name = :name AND b.url = :url")
      .setParameter("name", name)
      .setParameter("url", url)
      .uniqueResult().asInstanceOf[Bookmark])
  }

  override def listForUser(user: String): Iterable[Bookmark] = {
    getSession().createQuery("SELECT b.id, b.name, b.url FROM Bookmark b, BookmarkUser bu, User u WHERE " +
      "b.id = bu.bookmarkId AND bu.userId = u.id AND u.name = :user")
      .setParameter("user", user)
        .setResultTransformer(new ResultTransformer {
          override def transformList(collection: util.List[_]): util.List[_] = collection

          override def transformTuple(tuple: Array[AnyRef], aliases: Array[String]): AnyRef = {
            val ret = new Bookmark
            ret.id = tuple(0).asInstanceOf[Long]
            ret.name = tuple(1).asInstanceOf[String]
            ret.url = tuple(2).asInstanceOf[String]
            ret
          }
        })
      .list().asScala.map(_.asInstanceOf[Bookmark])
  }

  override def findByIdForUser(id: Long, user: String): Option[Bookmark] = {
    Option(getSession().createQuery("SELECT b.id, b.name, b.url FROM Bookmark b, BookmarkUser bu, User u WHERE " +
      "b.id = bu.bookmarkId AND bu.userId = u.id AND b.id = :id AND u.name = :user")
      .setParameter("id", id)
      .setParameter("user", user)
      .setResultTransformer(new ResultTransformer {
        override def transformList(collection: util.List[_]): util.List[_] = collection

        override def transformTuple(tuple: Array[AnyRef], aliases: Array[String]): AnyRef = {
          val ret = new Bookmark
          ret.id = tuple(0).asInstanceOf[Long]
          ret.name = tuple(1).asInstanceOf[String]
          ret.url = tuple(2).asInstanceOf[String]
          ret
        }
      })
      .uniqueResult().asInstanceOf[Bookmark])
  }
}
