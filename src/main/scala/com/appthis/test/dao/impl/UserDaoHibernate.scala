package com.appthis.test.dao.impl

import com.appthis.test.dao.UserDao
import com.appthis.test.dao.entities.User
import org.springframework.stereotype.Repository

/**
  * url-shortener
  *
  * @author Pavel
  * @since 08-Jul-17
  */
@Repository
class UserDaoHibernate extends AbstractDao[User] with UserDao{
  override def findByName(name: String): Option[User] = {
    Option(getSession().createQuery("FROM User u WHERE u.name = :name")
      .setParameter("name", name)
      .uniqueResult().asInstanceOf[User])
  }
}
