package com.appthis.test.dao.impl

import com.appthis.test.dao.BookmarkUserDao
import com.appthis.test.dao.entities.BookmarkUser
import org.springframework.stereotype.Repository

import scala.collection.JavaConverters._

/**
  * url-shortener
  *
  * @author Pavel
  * @since 08-Jul-17
  */
@Repository
class BookmarkUserDaoHibernate extends AbstractDao[BookmarkUser] with BookmarkUserDao {
  override def listByUserId(userId: Long): Iterable[BookmarkUser] = {
    getSession().createQuery("FROM BookmarkUser bu WHERE bu.userId = :userId")
      .setParameter("userId", userId)
        .list().asScala.map(_.asInstanceOf[BookmarkUser])
  }

  override def listByBookmarkId(bookmarkId: Long): Iterable[BookmarkUser] = {
    getSession().createQuery("FROM BookmarkUser bu WHERE bu.bookmarkId = :bookmarkId")
      .setParameter("bookmarkId", bookmarkId)
      .list().asScala.map(_.asInstanceOf[BookmarkUser])
  }
}
