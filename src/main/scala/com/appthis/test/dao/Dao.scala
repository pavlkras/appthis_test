package com.appthis.test.dao

import java.io

/**
  * url-shortener
  *
  * @author Pavel
  * @since 07-Jul-17
  */
trait Dao[E] {
  def findById(id: io.Serializable): Option[E]
  def save(entity: E): io.Serializable
  def list(): Iterable[E]
  def delete(id: io.Serializable): Unit
  def delete(entity: E): Unit
}
