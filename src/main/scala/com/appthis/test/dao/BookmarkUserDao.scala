package com.appthis.test.dao

import com.appthis.test.dao.entities.BookmarkUser

/**
  * url-shortener
  *
  * @author Pavel
  * @since 08-Jul-17
  */
trait BookmarkUserDao extends Dao[BookmarkUser] {
  def listByUserId(userId: Long): Iterable[BookmarkUser]
  def listByBookmarkId(bookmarkId: Long): Iterable[BookmarkUser]
}
