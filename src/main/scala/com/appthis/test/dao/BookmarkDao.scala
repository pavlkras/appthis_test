package com.appthis.test.dao

import java.io

import com.appthis.test.dao.entities.Bookmark

/**
  * url-shortener
  *
  * @author Pavel
  * @since 07-Jul-17
  */
trait BookmarkDao extends Dao[Bookmark]{
  def findByNameUrl(name: String, url: String): Option[Bookmark]
  def delete(name: String, url: String): Unit
  def listForUser(user: String): Iterable[Bookmark]
  def findByIdForUser(id: Long, user: String): Option[Bookmark]
}
