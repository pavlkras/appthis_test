package com.appthis.test.dao

import com.appthis.test.dao.entities.Role

/**
  * url-shortener
  *
  * @author Pavel
  * @since 08-Jul-17
  */
trait RoleDao extends Dao[Role]{
  def findByRole(role: String): Option[Role]
}
