package com.appthis.test.services.exceptions

/**
  * url-shortener
  *
  * @author Pavel
  * @since 07-Jul-17
  */
class ResourceSavingException(message: String) extends RuntimeException(message){

}
