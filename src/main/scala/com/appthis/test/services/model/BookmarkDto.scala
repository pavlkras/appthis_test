package com.appthis.test.services.model

import scala.beans.BeanProperty

/**
  * url-shortener
  *
  * @author Pavel
  * @since 07-Jul-17
  */
class BookmarkDto extends Serializable{
  @BeanProperty
  var name: String = _

  @BeanProperty
  var url: String = _

  @BeanProperty
  var shortenedUrl: String = _

}