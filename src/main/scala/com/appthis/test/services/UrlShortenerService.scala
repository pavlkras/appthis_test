package com.appthis.test.services

import com.appthis.test.services.model.BookmarkDto

/**
  * url-shortener
  *
  * @author Pavel
  * @since 07-Jul-17
  */
trait UrlShortenerService {
  def recreateUrlFromShortenedForUser(shortenedUrl: String, user: String): String

  def deleteBookmarkForUser(shortenedUrl: String, user: String): Unit

  def deleteBookmarkForUser(name: String, url: String, user: String): Unit

  def listAllForUser(user: String): scala.Iterable[BookmarkDto]

  def addBookmarkForUser(name: String, url: String, user: String): String

}
