package com.appthis.test.services

import com.appthis.test.dao.entities.User

/**
  * url-shortener
  *
  * @author Pavel
  * @since 08-Jul-17
  */
trait UserService {
  def findUserByName(name: String): Option[User]
  def saveUser(user: User): Unit
}
