package com.appthis.test.services

import scala.collection.mutable.ListBuffer

/**
  * url-shortener
  *
  * @author Pavel
  * @since 08-Jul-17
  */
object UrlEncoder {

  private val LETTERS: String = "abcdefghijklmnopqrstuvwxyzABXDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

  def id2Url(id: Long): String = {
    val buffer = new ListBuffer[Long]
    val lettersLen = LETTERS.length
    var calculatedValue = id

    while (calculatedValue > 0) {
      val reminder = calculatedValue % lettersLen
      buffer += reminder
      calculatedValue /= lettersLen
    }

    new String(buffer.map(i => LETTERS.charAt(i.toInt)).toArray)
  }

  def url2Id(url: String): Long = {

    var id: Long = 0
    val urlLength = url.length
    val lettersLength = LETTERS.length
    for (i <- 0 until urlLength) {
      id += (LETTERS.indexOf(url.charAt(i)) * scala.math.pow(lettersLength.toDouble, i.toDouble).toLong)
    }
    id
  }
}
