package com.appthis.test.services

import java.net.MalformedURLException

/**
  * url-shortener
  *
  * @author Pavel
  * @since 07-Jul-17
  */
trait UrlValidationService {

  @throws[MalformedURLException]
  def validateUrl(url: String): Boolean

}
