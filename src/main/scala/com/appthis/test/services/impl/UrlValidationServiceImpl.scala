package com.appthis.test.services.impl

import com.appthis.test.services.UrlValidationService
import org.apache.commons.validator.routines.UrlValidator
import org.springframework.stereotype.Service

/**
  * url-shortener
  *
  * @author Pavel
  * @since 07-Jul-17
  */
@Service
class UrlValidationServiceImpl extends UrlValidationService{
  override def validateUrl(url: String): Boolean = new UrlValidator().isValid(url)
}
