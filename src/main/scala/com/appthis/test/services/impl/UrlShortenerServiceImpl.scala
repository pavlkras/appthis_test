package com.appthis.test.services.impl

import java.net.MalformedURLException
import javax.inject.Inject
import javax.transaction.Transactional

import com.appthis.test.dao.{BookmarkDao, BookmarkUserDao, UserDao}
import com.appthis.test.dao.entities.{Bookmark, BookmarkUser}
import com.appthis.test.services.exceptions.{InvalidParametersException, ResourceSavingException}
import com.appthis.test.services.{HostResolver, UrlEncoder, UrlShortenerService}
import com.appthis.test.services.model.BookmarkDto
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.stereotype.Service

/**
  * url-shortener
  *
  * @author Pavel
  * @since 07-Jul-17
  */
@Service
class UrlShortenerServiceImpl @Inject()(bookmarkDao: BookmarkDao,
                                        userDao: UserDao,
                                        bookmarkUserDao: BookmarkUserDao) extends UrlShortenerService {

  @Transactional
  override def deleteBookmarkForUser(shortenedUrl: String, user: String): Unit = {
    val id = UrlEncoder.url2Id(shortenedUrl)
    val bookmark = bookmarkDao.findById(id)

    deleteBookmarkForUser(bookmark, user)
  }

  @Transactional
  override def deleteBookmarkForUser(name: String, url: String, userName: String): Unit = {
    val bookmark = bookmarkDao.findByNameUrl(name, url)

    deleteBookmarkForUser(bookmark, userName)
  }

  override def listAllForUser(user: String): Iterable[BookmarkDto] = {
    bookmarkDao.listForUser(user).map(e => {
      val dto = new BookmarkDto
      dto.name = e.name
      dto.url = e.url
      dto.shortenedUrl = generateShortenedUrl(e.id)
      dto
    })
  }


  @Transactional
  override def addBookmarkForUser(name: String, url: String, userName: String): String = {
    val user = userDao.findByName(userName).get

    val storedBookmark = bookmarkDao.findByNameUrl(name, url)

    var bookmarkId: Long = -1
    if (storedBookmark.isEmpty) {
      val bookmark = new Bookmark
      bookmark.name = name
      bookmark.url = url
      bookmarkId = bookmarkDao.save(bookmark).asInstanceOf[Long]
    } else {
      bookmarkId = storedBookmark.get.id
    }

    val bookmarkUser = new BookmarkUser
    bookmarkUser.userId = user.id
    bookmarkUser.bookmarkId = bookmarkId
    try {
      bookmarkUserDao.save(bookmarkUser)
    } catch {
      case dve: DataIntegrityViolationException =>
    }

    generateShortenedUrl(bookmarkUser.bookmarkId)
  }


  override def recreateUrlFromShortenedForUser(shortenedUrl: String, user: String): String = {
    val id = UrlEncoder.url2Id(shortenedUrl)
    bookmarkDao.findByIdForUser(id, user) match {
      case Some(bookmark: Bookmark) => bookmark.url
      case None => throw new InvalidParametersException("URL does not exist for this user")
    }
  }

  private def deleteBookmarkForUser(bookmark: Option[Bookmark], userName: String): Unit = {

    val bookmarkId = bookmark match {
      case Some(_) => bookmark.get.id
      case None => throw new ResourceSavingException("Bookmark does not exist")
    }

    val userId = userDao.findByName(userName).get.id

    val usersToBookmark = bookmarkUserDao.listByBookmarkId(bookmarkId)
    usersToBookmark.size match {
      case 0 => throw new ResourceSavingException("Bookmark is not assigned to user")
      case 1 => val bookmarkUser = usersToBookmark.head
        if (bookmarkUser.userId == userId && bookmarkUser.bookmarkId == bookmarkId) {
          bookmarkUserDao.delete(bookmarkUser)
          bookmarkDao.delete(bookmark.get)
        }
      case _ => usersToBookmark.find(e => e.userId == userId && e.bookmarkId == bookmarkId) match {
        case Some(entity) => bookmarkUserDao.delete(entity)
        case None => throw new ResourceSavingException("Bookmark is not assigned to user")
      }
    }
  }

  private def resolveMyHost(): String = HostResolver.getMyHostAsString

  private def generateShortenedUrl(id: Long): _root_.scala.Predef.String = resolveMyHost() + UrlEncoder.id2Url(id)
}