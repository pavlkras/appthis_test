package com.appthis.test.services.impl

import javax.inject.Inject
import javax.transaction.Transactional

import com.appthis.test.dao.{RoleDao, UserDao}
import com.appthis.test.dao.entities.{Role, User}
import com.appthis.test.services.UserService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service

/**
  * url-shortener
  *
  * @author Pavel
  * @since 08-Jul-17
  */
@Service
class UserServiceImpl @Inject()(userDao: UserDao,
                                roleDao: RoleDao,
                                bCryptPasswordEncoder: BCryptPasswordEncoder) extends UserService{

  private val ROLE_DEFAULT: String = "USER"

  override def findUserByName(name: String): Option[User] = {
    userDao.findByName(name)
  }

  @Transactional
  override def saveUser(user: User): Unit = {
    user.password = bCryptPasswordEncoder.encode(user.password)
    val id = userDao.save(user).asInstanceOf[Long]
    val role = new Role
    role.id = id
    role.role = ROLE_DEFAULT
    roleDao.save(role)
  }
}
