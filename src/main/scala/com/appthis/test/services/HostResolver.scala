package com.appthis.test.services

import java.io.{BufferedReader, InputStreamReader}
import java.net.URL

/**
  * url-shortener
  *
  * @author Pavel
  * @since 08-Jul-17
  */
object HostResolver {
  def getMyHostAsString: String = {
    var host: String = ""

    val myIp = new URL("http://checkip.amazonaws.com")
    val in = new BufferedReader(new InputStreamReader(myIp.openStream()))
    try {
      host = s"http://${in.readLine()}:8080/"
    } finally {
      in.close()
    }

    host
  }
}
